#!/bin/sh

# Receive the minimum charge as an argument
MIN_CHARGE=$1

while [ true ]; do
CHARGE=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep "percentage" | awk '{print $2}' | cut -d '%' -f1)
STATUS=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep "state" | awk '{print $2}') 

if [[ $CHARGE < $MIN_CHARGE ]]; then
  if [[ $STATUS != "charging" ]]; then
     /usr/bin/notify-send -u critical "WARNING: Battery has fallen below $MIN_CHARGE%" "Please recharge your laptop as soon as possible!"
  fi
fi 
sleep 60
done
