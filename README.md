# battery.sh

Simple script to send a notification when your laptop's battery is below a percentage, to be used as a background job.

# How to install:

Clone this repo: 

```sh
git clone "https://gitlab.com/vladlu/battery.sh.git"
```

Now, add it as a daemon to your system as you prefer! You may want to use it as an init service or when you call your desktop: and you can do it all! The possibilities are **ENDLESS**!

# How to use:

You simply call it with a number in front of it. That mumber will be the percentage at which you will be sent the message.
```sh
/path/to/battery.sh 25 # sets notification to end at 25%

```
